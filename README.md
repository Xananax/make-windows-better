# Make Windows Better

Windows 10 is a pretty decent operating system. It has two problems:

1. it spies on you, forces you to sign in with a Microsoft account (workaround: disconnect during install)
2. it does a bunch of things without your permission or will (workarounds below in this document)
3. it has incredible UX shortcomings and omissions.

This is a set of scripts, mods and software to make Windows more usable.

## Get [Scoop](https://scoop.sh/)

In an elevated powershell:

```powershell
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
scoop bucket add extras
scoop install gcc python scons yasm make
```

## Get [Chocolatey](https://chocolatey.org/)

Install it; it's just 2 commands to run in powershell.

Then, install the stuff you need from an admin PowerShell

```powershell
choco feature enable -n allowGlobalConfirmation
```

```powershell
# arting
choco install krita gimp mypaint inkscape
```

```powershell
# utilities
choco install copyq rufus vlc qbittorrent 7zip notepadplusplus.install sysinternals procexp renamer jbs keepass lockhunter sharex linux-reader
```

```powershell
# internet stuff
# (chromium is Chrome but without the Google splying stuff)
firefox chromium mailspring
```

```powershell
# programming
choco install python git nvm nodejs.install cmder vscode virtualbox godot hyper mobaxterm
choco install docker-desktop --pre
choco install openjdk androidstudio windows-sdk-10.0 visualstudio2019community dotnetcore-sdk
```

```powershell
# gaming
choco install playnite steam gog itch goggalaxy
```

For some reason we need to install pip after python

```powershell
choco install pip
```

## DLLs

Download `vc_redist.x64.exe` and/or `vc_redist.x86.exe`. You will need them to run a ton of things

https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads

## Better shortcuts

Git clone, or download this repo. Then, run this in an admin powershell:

```powershell
# Run the following commands in an Administrator powershell prompt. 
# Be sure to specify the correct path to this repo. 
$location = "C:\Users\xananax\Documents\MakeWindowsBetter\"

$A = New-ScheduledTaskAction -Execute "$location\autohotkey.ahk" -WorkingDirectory $location
$T = New-ScheduledTaskTrigger -AtLogon 
$P = New-ScheduledTaskPrincipal -GroupId "BUILTIN\Administrators" -RunLevel Highest
$S = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit 0
$D = New-ScheduledTask -Action $A -Principal $P -Trigger $T -Settings $S
Register-ScheduledTask WindowsDesktopSwitcher -InputObject $D
```

Now, you can use:

- <kbd>win</kbd>+<kbd>n</kbd> (where `n` is a number) to jump to that desktop
- Pressing the same key again takes you to the previous Desktop (if you were on Desktop 3, and pressed <kbd>win</kbd>+<kbd>1</kbd>, you can press <kbd>1</kbd> again to go back where you were)
- <kbd>win</kbd>+<kbd>shift</kbd>+<kbd>n</kbd> to send a window to that desktop
- <kbd>win</kbd>+<kbd>←</kbd> or <kbd>win</kbd>+<kbd>→</kbd> go previous/next desktop
- <kbd>win</kbd>+<kbd>shift</kbd>+<kbd>space</kbd> toggle a window's maximized state
- <kbd>win</kbd>+<kbd>shift</kbd>+<kbd>q</kbd> close a window
- <kbd>win</kbd>+<kbd>f</kbd> open the file manager in your home
- <kbd>win</kbd>+<kbd>⏎</kbd> opens powershell
- <kbd>win</kbd>+<kbd>shift</kbd>+<kbd>⏎</kbd> opens admin powershell

Additionally, you can use:

- <kbd>Caps</kbd>+<kbd>←</kbd> to jump to the start of the line
- <kbd>Caps</kbd>+<kbd>→</kbd> to jump to the end of the line

That can be useful on laptops that, like mine, have difficult `home` and `end` keys.
The CapsLock functionality is preserved, you have to double tap <kbd>Caps</kbd> to toggle it.

## Keyboard

Turns out, having a decent keyboard on Windows is possible. It exists, it's just not official. It works great though, give it a try:

https://www.scottseverance.us/html/keyboard/win_intl_altgr/

The installation is included in this repo, just run `installations/win-intl-altgr-setup.exe`, then log out, come back, pick the new keyboard in the language options.

Now, your right <kbd>alt</kbd> is a modifier.  
To do an `é`, you can press <kbd>alt</kbd>+<kbd>'</kbd>, then <kbd>e</kbd>. In the specific case of `é`, you can also just press <kbd>alt</kbd>+<kbd>e</kbd>. This is a very flexible system, which allows all accents without fiddling with `alt+130` codes, nor having dead keys.

Enjoy!

## Better Launcher

I'm using [keypirinha](http://keypirinha.com/)

Config:

```ini
[app]
launch_at_startup = yes
hotkey_run = Win+Q
[gui]
always_on_top = yes
hide_on_focus_lost = yes
escape_always_closes = yes
show_on_taskbar = no
```

## Better Focus

To make focus follow mouse, run this in an elevated powershell:

```powershell
Add-Type -TypeDefinition @'
    using System;
    using System.Runtime.InteropServices;
    using System.ComponentModel;

    public static class Spi {
        [System.FlagsAttribute]
        private enum Flags : uint {
            None            = 0x0,
            UpdateIniFile   = 0x1,
            SendChange      = 0x2,
        }

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool SystemParametersInfo(
            uint uiAction, uint uiParam, UIntPtr pvParam, Flags flags );

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool SystemParametersInfo(
            uint uiAction, uint uiParam, out bool pvParam, Flags flags );

        private static void check( bool ok ) {
            if( ! ok )
                throw new Win32Exception( Marshal.GetLastWin32Error() );
        }

        private static UIntPtr ToUIntPtr( this bool value ) {
            return new UIntPtr( value ? 1u : 0u );
        }

        public static bool GetActiveWindowTracking() {
            bool enabled;
            check( SystemParametersInfo( 0x1000, 0, out enabled, Flags.None ) );
            return enabled;
        }

        public static void SetActiveWindowTracking( bool enabled ) {
            // note: pvParam contains the boolean (cast to void*), not a pointer to it!
            check( SystemParametersInfo( 0x1001, 0, enabled.ToUIntPtr(), Flags.SendChange ) );
        }
    }
'@

# check if mouse-focus is enabled
[Spi]::GetActiveWindowTracking()

# disable mouse-focus (default)
[Spi]::SetActiveWindowTracking( $false )

# enable mouse-focus
[Spi]::SetActiveWindowTracking( $true )
```

This needs to be re-run every time you restart Windows.


## Other Windows Tweaks

Small things that add up to a better experience

### Disable superfetch

Superfetch is useless if you have an SSD, and it hogs system resources

run `cmd` as administrator

```cmd
sc stop "SysMain" & sc config "SysMain" start=disabled
```

### Disable Game Bar & DVR

- Open Windows Settings (<kbd>win</kbd>+<kbd>i</kbd>)
- go to `Gaming` > `Game Bar`, turn off
- go to `Gaming` > `Captures`, turn off

### Tell Windows to Stop Hogging Your Connection

- Open Windows Settings (<kbd>win</kbd>+<kbd>i</kbd>)
- Go to `Network & Internet` > `Wi-Fi`.
- Select the network you’re currently connected to and under `Metered connection` > `Set as metered connection` turn the switch On.

### Tell Windows Update to not Install Stuff When you Don't Want it To

- Type `Windows Update settings` into the Windows Search Bar and run the configuration utility.
- Under the heading `Update settings`, choose `Change active hours`.
- You can change the active hours on this menu by clicking on `Change Active Hours`. I advise changing it a time when the computer is on but unused.

### Limit Cortana

Run the registry file in `./registry`

### Disable <kbd>win</kbd>+x shortcuts

Run the registry file in `./registry`

### Access Tweaks

To enable God Mode, create a new folder and leave it empty. (I like to plop God Mode on my desktop.) Then right-click the folder, select Rename, and give it the following name:

`God Mode.{ED7BA470-8E54-465E-825C-99712043E01C}.`

### Apply Black Theme?

```powershell
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name AppsUseLightTheme -Value 0
```

Log out.

If you wanna go back to the regular theme, run:

```powershell
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name AppsUseLightTheme -Value 1
```
